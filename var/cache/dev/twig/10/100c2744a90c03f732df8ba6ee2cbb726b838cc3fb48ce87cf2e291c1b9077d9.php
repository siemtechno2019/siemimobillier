<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* owner/show.html.twig */
class __TwigTemplate_42b61ffbb41dbdad607bc810eb6b991a3fc3de2308e241b2af5ae53b27ac500b extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "owner.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "owner/show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "owner/show.html.twig"));

        $this->parent = $this->loadTemplate("owner.html.twig", "owner/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<div class=\"jumbotron text-center \">
   <div class=\"container\">
        <div class =\"row\">
            <div class=\"col-md-8\">
                
               
            </div>
            <div class=\"col-md-4\">
                
                <h3> ";
        // line 14
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["appartement"]) || array_key_exists("appartement", $context) ? $context["appartement"] : (function () { throw new RuntimeError('Variable "appartement" does not exist.', 14, $this->source); })()), "quartier", [], "any", false, false, false, 14), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["appartement"]) || array_key_exists("appartement", $context) ? $context["appartement"] : (function () { throw new RuntimeError('Variable "appartement" does not exist.', 14, $this->source); })()), "ville", [], "any", false, false, false, 14), "html", null, true);
        echo ")</h3>
                <h2><div class=\"text-primary\">";
        // line 15
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["appartement"]) || array_key_exists("appartement", $context) ? $context["appartement"] : (function () { throw new RuntimeError('Variable "appartement" does not exist.', 15, $this->source); })()), "prix", [], "any", false, false, false, 15), "html", null, true);
        echo " FCFA </div></h2>
            </div>
        </div>
    </div>
</div>
</div>
<div class=\"container\">
        <p>";
        // line 22
        echo nl2br(twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["appartement"]) || array_key_exists("appartement", $context) ? $context["appartement"] : (function () { throw new RuntimeError('Variable "appartement" does not exist.', 22, $this->source); })()), "descrition", [], "any", false, false, false, 22), "html", null, true));
        echo "</p>

        <div class= \"row \">
             <div class=\"col-md-12\">
                <h2>Carateristiques</h2>
                <table class= \"table table-striped \">
                    
                    <tr>   
                        <td>Nombre de pièces </td>
                        <td>";
        // line 31
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["appartement"]) || array_key_exists("appartement", $context) ? $context["appartement"] : (function () { throw new RuntimeError('Variable "appartement" does not exist.', 31, $this->source); })()), "nbPieces", [], "any", false, false, false, 31), "html", null, true);
        echo "</td>
                    </tr>
                    <tr>   
                        <td>Chambres</td>
                        <td>";
        // line 35
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["appartement"]) || array_key_exists("appartement", $context) ? $context["appartement"] : (function () { throw new RuntimeError('Variable "appartement" does not exist.', 35, $this->source); })()), "nbreChambres", [], "any", false, false, false, 35), "html", null, true);
        echo "</td>
                    </tr>
                    
                     <tr>   
                        <td>type</td>
                        <td>";
        // line 40
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["appartement"]) || array_key_exists("appartement", $context) ? $context["appartement"] : (function () { throw new RuntimeError('Variable "appartement" does not exist.', 40, $this->source); })()), "types", [], "any", false, false, false, 40), "html", null, true);
        echo "</td>
                    </tr>
                </table> 
            </div>
</div>
    </div>
    <div class=\"row\">
        <div class=\"col-sm-12 text-right\">
                
                <a href=\"";
        // line 49
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("owner");
        echo "\" class=\"btn btn-info btn-sm mr-4\">
                    <span><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i> </span>
                    Retour à la liste
                </a>
        </div>
    </div>
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "owner/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  135 => 49,  123 => 40,  115 => 35,  108 => 31,  96 => 22,  86 => 15,  80 => 14,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'owner.html.twig' %}

{% block body %}

<div class=\"jumbotron text-center \">
   <div class=\"container\">
        <div class =\"row\">
            <div class=\"col-md-8\">
                
               
            </div>
            <div class=\"col-md-4\">
                
                <h3> {{appartement.quartier}} ({{appartement.ville}})</h3>
                <h2><div class=\"text-primary\">{{appartement.prix}} FCFA </div></h2>
            </div>
        </div>
    </div>
</div>
</div>
<div class=\"container\">
        <p>{{appartement.descrition|nl2br}}</p>

        <div class= \"row \">
             <div class=\"col-md-12\">
                <h2>Carateristiques</h2>
                <table class= \"table table-striped \">
                    
                    <tr>   
                        <td>Nombre de pièces </td>
                        <td>{{appartement.nbPieces}}</td>
                    </tr>
                    <tr>   
                        <td>Chambres</td>
                        <td>{{appartement.nbreChambres}}</td>
                    </tr>
                    
                     <tr>   
                        <td>type</td>
                        <td>{{appartement.types}}</td>
                    </tr>
                </table> 
            </div>
</div>
    </div>
    <div class=\"row\">
        <div class=\"col-sm-12 text-right\">
                
                <a href=\"{{ path('owner') }}\" class=\"btn btn-info btn-sm mr-4\">
                    <span><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i> </span>
                    Retour à la liste
                </a>
        </div>
    </div>
</div>

{% endblock %} 
", "owner/show.html.twig", "E:\\Work\\siems\\Siem immo\\templates\\owner\\show.html.twig");
    }
}
