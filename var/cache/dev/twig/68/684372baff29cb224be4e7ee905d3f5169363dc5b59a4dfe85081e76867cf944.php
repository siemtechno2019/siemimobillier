<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/detail.html.twig */
class __TwigTemplate_5a6eaeb5cf38e5c2ec80cb4e9c8308572768f17bdce7743725fe85b1e1f30da4 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 2
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/detail.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/detail.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "home/detail.html.twig", 2);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 5
        echo "
<div class=\"jumbotron text-center \">
   <div class=\"container\">
        <div class =\"row\">
            <div class=\"col-md-8\">
                
               
                
            </div>
            <div class=\"col-md-4 mt-4\">
                
                <h1> ";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["appartement"]) || array_key_exists("appartement", $context) ? $context["appartement"] : (function () { throw new RuntimeError('Variable "appartement" does not exist.', 16, $this->source); })()), "quartier", [], "any", false, false, false, 16), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["appartement"]) || array_key_exists("appartement", $context) ? $context["appartement"] : (function () { throw new RuntimeError('Variable "appartement" does not exist.', 16, $this->source); })()), "ville", [], "any", false, false, false, 16), "html", null, true);
        echo ")</h1>
                <h2><div class=\"text-primary\">";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["appartement"]) || array_key_exists("appartement", $context) ? $context["appartement"] : (function () { throw new RuntimeError('Variable "appartement" does not exist.', 17, $this->source); })()), "prix", [], "any", false, false, false, 17), "html", null, true);
        echo " FCFA </div></h2>
            </div>
        </div>
    </div>
</div>
</div>
<div class=\"container\">
        <p>";
        // line 24
        echo nl2br(twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["appartement"]) || array_key_exists("appartement", $context) ? $context["appartement"] : (function () { throw new RuntimeError('Variable "appartement" does not exist.', 24, $this->source); })()), "descrition", [], "any", false, false, false, 24), "html", null, true));
        echo "</p>

        <div class= \"row \">
             <div class=\"col-md-12\">
                <h2>Carateristiques</h2>
                <table class= \"table table-striped \">
                    
                    <tr>   
                        <td>Nombre de pièces </td>
                        <td>";
        // line 33
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["appartement"]) || array_key_exists("appartement", $context) ? $context["appartement"] : (function () { throw new RuntimeError('Variable "appartement" does not exist.', 33, $this->source); })()), "nbPieces", [], "any", false, false, false, 33), "html", null, true);
        echo "</td>
                    </tr>
                    <tr>   
                        <td>Chambres</td>
                        <td>";
        // line 37
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["appartement"]) || array_key_exists("appartement", $context) ? $context["appartement"] : (function () { throw new RuntimeError('Variable "appartement" does not exist.', 37, $this->source); })()), "nbreChambres", [], "any", false, false, false, 37), "html", null, true);
        echo "</td>
                    </tr>
                    
                     <tr>   
                        <td>type</td>
                        <td>";
        // line 42
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["appartement"]) || array_key_exists("appartement", $context) ? $context["appartement"] : (function () { throw new RuntimeError('Variable "appartement" does not exist.', 42, $this->source); })()), "types", [], "any", false, false, false, 42), "html", null, true);
        echo "</td>
                    </tr>
                </table> 
            </div>
</div>
    </div>
   
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "home/detail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 42,  116 => 37,  109 => 33,  97 => 24,  87 => 17,  81 => 16,  68 => 5,  58 => 4,  35 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("
{% extends 'base.html.twig' %}

{% block body %}

<div class=\"jumbotron text-center \">
   <div class=\"container\">
        <div class =\"row\">
            <div class=\"col-md-8\">
                
               
                
            </div>
            <div class=\"col-md-4 mt-4\">
                
                <h1> {{appartement.quartier}} ({{appartement.ville}})</h1>
                <h2><div class=\"text-primary\">{{appartement.prix}} FCFA </div></h2>
            </div>
        </div>
    </div>
</div>
</div>
<div class=\"container\">
        <p>{{appartement.descrition|nl2br}}</p>

        <div class= \"row \">
             <div class=\"col-md-12\">
                <h2>Carateristiques</h2>
                <table class= \"table table-striped \">
                    
                    <tr>   
                        <td>Nombre de pièces </td>
                        <td>{{appartement.nbPieces}}</td>
                    </tr>
                    <tr>   
                        <td>Chambres</td>
                        <td>{{appartement.nbreChambres}}</td>
                    </tr>
                    
                     <tr>   
                        <td>type</td>
                        <td>{{appartement.types}}</td>
                    </tr>
                </table> 
            </div>
</div>
    </div>
   
</div>

{% endblock %} 
", "home/detail.html.twig", "E:\\Work\\siems\\Siem immo\\templates\\home\\detail.html.twig");
    }
}
