<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* test_admin.html.twig */
class __TwigTemplate_91cd2d32160907282f3d60199d562b898802011bdca19c5f7eb10d84a06dc2b4 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "test_admin.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "test_admin.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
  <title>Matrix Admin</title>
  <meta charset=\"UTF-8\" />
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
  <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\HttpFoundationExtension']->generateAbsoluteUrl($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/bootstrap.min.css")), "html", null, true);
        echo "\">
  <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\HttpFoundationExtension']->generateAbsoluteUrl($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/bootstrap-responsive.min.css")), "html", null, true);
        echo "\">
  <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\HttpFoundationExtension']->generateAbsoluteUrl($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/fullcalendar.css")), "html", null, true);
        echo "\">
  <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\HttpFoundationExtension']->generateAbsoluteUrl($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/matrix-style.css")), "html", null, true);
        echo "\">
  <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\HttpFoundationExtension']->generateAbsoluteUrl($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/matrix-media.css")), "html", null, true);
        echo "\">
  <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\HttpFoundationExtension']->generateAbsoluteUrl($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("font-awesome/css/font-awesome.css")), "html", null, true);
        echo "\">
  <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\HttpFoundationExtension']->generateAbsoluteUrl($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/fullcalendar.css")), "html", null, true);
        echo "\">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

</head>
<body>

<!--Header-part-->
<div id=\"header\">
  <h1><a href=\"dashboard.html\">Matrix Admin</a></h1>
</div>
<!--close-Header-part--> 


<!--top-Header-menu-->
<div id=\"user-nav\" class=\"navbar navbar-expand-md navbar-dark fixed-top bg-dark\">
  <ul class=\"nav\">
    <li  class=\"dropdown\" id=\"profile-messages\" ><a title=\"\" href=\"#\" data-toggle=\"dropdown\" data-target=\"#profile-messages\" class=\"dropdown-toggle\"><i class=\"icon icon-user\"></i>  <span class=\"text\">Welcome User</span><b class=\"caret\"></b></a>
      <ul class=\"dropdown-menu\">
        <li><a href=\"#\"><i class=\"icon-user\"></i> Mon Profil</a></li>
        <li class=\"divider\"></li>
        <li><a href=\"#\"><i class=\"icon-check\"></i> Mes Taches</a></li>
        <li class=\"divider\"></li>
      </ul>
    </li>
    <li class=\"dropdown\" id=\"menu-messages\"><a href=\"#\" data-toggle=\"dropdown\" data-target=\"#menu-messages\" class=\"dropdown-toggle\"><i class=\"icon icon-envelope\"></i> <span class=\"text\">Messages</span> <span class=\"label label-important\">5</span> <b class=\"caret\"></b></a>
      <ul class=\"dropdown-menu\">
        <li><a class=\"sAdd\" title=\"\" href=\"#\"><i class=\"icon-plus\"></i> nouveau message</a></li>
        <li class=\"divider\"></li>
        <li><a class=\"sInbox\" title=\"\" href=\"#\"><i class=\"icon-envelope\"></i> inbox</a></li>
        <li class=\"divider\"></li>
        <li><a class=\"sOutbox\" title=\"\" href=\"#\"><i class=\"icon-arrow-up\"></i> outbox</a></li>
        <li class=\"divider\"></li>
        <li><a class=\"sTrash\" title=\"\" href=\"#\"><i class=\"icon-trash\"></i> trash</a></li>
      </ul>
    </li>
    <li class=\"\"><a title=\"\" href=\"login.html\"><i class=\"icon icon-share-alt\"></i> <span class=\"text\">Se connecter</span></a></li>
  </ul>
</div>
<!--close-top-Header-menu-->

<!--start-top-serch-->
<div id=\"search\">
  <input type=\"text\" placeholder=\"Search here...\"/>
  <button type=\"submit\" class=\"tip-bottom\" title=\"Search\"><i class=\"icon-search icon-white\"></i></button>
</div>
<!--close-top-serch-->
<!--sidebar-menu-->
<div id=\"sidebar\"><a href=\"#\" class=\"visible-phone\"><i class=\"icon icon-home\"></i> Dashboard</a>
  <ul>
    <li class=\"active\"><a href=\"index.html\"><i class=\"icon icon-home\"></i> <span>Dashboard</span></a> </li>
    <li class=\"submenu\"> <a href=\"#\"><i class=\"icon icon-file\"></i> <span>Addons</span> <span class=\"label label-important\">5</span></a>
      <ul>
        <li><a href=\"invoice.html\">Invoice</a></li>
      </ul>
    </li>
    
  </ul>
</div>
<!--sidebar-menu-->

<!--main-container-part-->
<div id=\"content\">
    ";
        // line 75
        $this->displayBlock('body', $context, $blocks);
        // line 80
        echo "</div>

<!--end-main-container-part-->

<!--Footer-part-->

<div class=\"row-fluid\">
  <div id=\"footer\" class=\"span12\"> 2019 &copy; Siem Immobilier. L'immobilier à votre porté'  </div>
</div>

<!--end-Footer-part-->

<script src=\"js/excanvas.min.js\"></script> 
<script src=\"js/jquery.min.js\"></script> 
<script src=\"js/jquery.ui.custom.js\"></script> 
<script src=\"js/bootstrap.min.js\"></script> 
<script src=\"js/jquery.flot.min.js\"></script> 
<script src=\"js/jquery.flot.resize.min.js\"></script> 
<script src=\"js/jquery.peity.min.js\"></script> 
<script src=\"js/fullcalendar.min.js\"></script> 
<script src=\"js/matrix.js\"></script> 
<script src=\"js/matrix.dashboard.js\"></script> 
<script src=\"js/jquery.gritter.min.js\"></script> 
<script src=\"js/matrix.interface.js\"></script> 
<script src=\"js/matrix.chat.js\"></script> 
<script src=\"js/jquery.validate.js\"></script> 
<script src=\"js/matrix.form_validation.js\"></script> 
<script src=\"js/jquery.wizard.js\"></script> 
<script src=\"js/jquery.uniform.js\"></script> 
<script src=\"js/select2.min.js\"></script> 
<script src=\"js/matrix.popover.js\"></script> 
<script src=\"js/jquery.dataTables.min.js\"></script> 
<script src=\"js/matrix.tables.js\"></script> 

<script type=\"text/javascript\">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != \"\") {
      
          // if url is \"-\", it is this page -- reset the menu:
          if (newURL == \"-\" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
</body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 75
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 76
        echo "    <div class=\"mt-4\">

    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "test_admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  223 => 76,  213 => 75,  143 => 80,  141 => 75,  76 => 13,  72 => 12,  68 => 11,  64 => 10,  60 => 9,  56 => 8,  52 => 7,  44 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">
<head>
  <title>Matrix Admin</title>
  <meta charset=\"UTF-8\" />
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
  <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ absolute_url(asset('css/bootstrap.min.css')) }}\">
  <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ absolute_url(asset('css/bootstrap-responsive.min.css')) }}\">
  <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ absolute_url(asset('css/fullcalendar.css')) }}\">
  <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ absolute_url(asset('css/matrix-style.css')) }}\">
  <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ absolute_url(asset('css/matrix-media.css')) }}\">
  <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ absolute_url(asset('font-awesome/css/font-awesome.css')) }}\">
  <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ absolute_url(asset('css/fullcalendar.css')) }}\">
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

</head>
<body>

<!--Header-part-->
<div id=\"header\">
  <h1><a href=\"dashboard.html\">Matrix Admin</a></h1>
</div>
<!--close-Header-part--> 


<!--top-Header-menu-->
<div id=\"user-nav\" class=\"navbar navbar-expand-md navbar-dark fixed-top bg-dark\">
  <ul class=\"nav\">
    <li  class=\"dropdown\" id=\"profile-messages\" ><a title=\"\" href=\"#\" data-toggle=\"dropdown\" data-target=\"#profile-messages\" class=\"dropdown-toggle\"><i class=\"icon icon-user\"></i>  <span class=\"text\">Welcome User</span><b class=\"caret\"></b></a>
      <ul class=\"dropdown-menu\">
        <li><a href=\"#\"><i class=\"icon-user\"></i> Mon Profil</a></li>
        <li class=\"divider\"></li>
        <li><a href=\"#\"><i class=\"icon-check\"></i> Mes Taches</a></li>
        <li class=\"divider\"></li>
      </ul>
    </li>
    <li class=\"dropdown\" id=\"menu-messages\"><a href=\"#\" data-toggle=\"dropdown\" data-target=\"#menu-messages\" class=\"dropdown-toggle\"><i class=\"icon icon-envelope\"></i> <span class=\"text\">Messages</span> <span class=\"label label-important\">5</span> <b class=\"caret\"></b></a>
      <ul class=\"dropdown-menu\">
        <li><a class=\"sAdd\" title=\"\" href=\"#\"><i class=\"icon-plus\"></i> nouveau message</a></li>
        <li class=\"divider\"></li>
        <li><a class=\"sInbox\" title=\"\" href=\"#\"><i class=\"icon-envelope\"></i> inbox</a></li>
        <li class=\"divider\"></li>
        <li><a class=\"sOutbox\" title=\"\" href=\"#\"><i class=\"icon-arrow-up\"></i> outbox</a></li>
        <li class=\"divider\"></li>
        <li><a class=\"sTrash\" title=\"\" href=\"#\"><i class=\"icon-trash\"></i> trash</a></li>
      </ul>
    </li>
    <li class=\"\"><a title=\"\" href=\"login.html\"><i class=\"icon icon-share-alt\"></i> <span class=\"text\">Se connecter</span></a></li>
  </ul>
</div>
<!--close-top-Header-menu-->

<!--start-top-serch-->
<div id=\"search\">
  <input type=\"text\" placeholder=\"Search here...\"/>
  <button type=\"submit\" class=\"tip-bottom\" title=\"Search\"><i class=\"icon-search icon-white\"></i></button>
</div>
<!--close-top-serch-->
<!--sidebar-menu-->
<div id=\"sidebar\"><a href=\"#\" class=\"visible-phone\"><i class=\"icon icon-home\"></i> Dashboard</a>
  <ul>
    <li class=\"active\"><a href=\"index.html\"><i class=\"icon icon-home\"></i> <span>Dashboard</span></a> </li>
    <li class=\"submenu\"> <a href=\"#\"><i class=\"icon icon-file\"></i> <span>Addons</span> <span class=\"label label-important\">5</span></a>
      <ul>
        <li><a href=\"invoice.html\">Invoice</a></li>
      </ul>
    </li>
    
  </ul>
</div>
<!--sidebar-menu-->

<!--main-container-part-->
<div id=\"content\">
    {% block body %}
    <div class=\"mt-4\">

    </div>
{% endblock %}
</div>

<!--end-main-container-part-->

<!--Footer-part-->

<div class=\"row-fluid\">
  <div id=\"footer\" class=\"span12\"> 2019 &copy; Siem Immobilier. L'immobilier à votre porté'  </div>
</div>

<!--end-Footer-part-->

<script src=\"js/excanvas.min.js\"></script> 
<script src=\"js/jquery.min.js\"></script> 
<script src=\"js/jquery.ui.custom.js\"></script> 
<script src=\"js/bootstrap.min.js\"></script> 
<script src=\"js/jquery.flot.min.js\"></script> 
<script src=\"js/jquery.flot.resize.min.js\"></script> 
<script src=\"js/jquery.peity.min.js\"></script> 
<script src=\"js/fullcalendar.min.js\"></script> 
<script src=\"js/matrix.js\"></script> 
<script src=\"js/matrix.dashboard.js\"></script> 
<script src=\"js/jquery.gritter.min.js\"></script> 
<script src=\"js/matrix.interface.js\"></script> 
<script src=\"js/matrix.chat.js\"></script> 
<script src=\"js/jquery.validate.js\"></script> 
<script src=\"js/matrix.form_validation.js\"></script> 
<script src=\"js/jquery.wizard.js\"></script> 
<script src=\"js/jquery.uniform.js\"></script> 
<script src=\"js/select2.min.js\"></script> 
<script src=\"js/matrix.popover.js\"></script> 
<script src=\"js/jquery.dataTables.min.js\"></script> 
<script src=\"js/matrix.tables.js\"></script> 

<script type=\"text/javascript\">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != \"\") {
      
          // if url is \"-\", it is this page -- reset the menu:
          if (newURL == \"-\" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
</body>
</html>
", "test_admin.html.twig", "E:\\Work\\siems\\Siem immo\\templates\\test_admin.html.twig");
    }
}
