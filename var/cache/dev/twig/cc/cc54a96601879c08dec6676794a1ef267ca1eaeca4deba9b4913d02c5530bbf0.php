<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* appartement/show.html.twig */
class __TwigTemplate_fa9ee6bbe6040ea4df401d8da187ace8f7d5a06e91fedbc4d0c3821f1444f449 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "appartement/show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "appartement/show.html.twig"));

        $this->parent = $this->loadTemplate("admin.html.twig", "appartement/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<div class=\"jumbotron text-center \">
   <div class=\"container\">
        <div class =\"row\">
            <div class=\"col-md-8\">
                
                ";
        // line 10
        if (twig_get_attribute($this->env, $this->source, (isset($context["appartement"]) || array_key_exists("appartement", $context) ? $context["appartement"] : (function () { throw new RuntimeError('Variable "appartement" does not exist.', 10, $this->source); })()), "filename", [], "any", false, false, false, 10)) {
            // line 11
            echo "                   
                    <img src=\"";
            // line 12
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("uploads/appartement/" . twig_get_attribute($this->env, $this->source, (isset($context["appartement"]) || array_key_exists("appartement", $context) ? $context["appartement"] : (function () { throw new RuntimeError('Variable "appartement" does not exist.', 12, $this->source); })()), "filename", [], "any", false, false, false, 12))), "html", null, true);
            echo "\" alt=\"img-card-top\"  style=\"width: 100%;height: 100%;\">
                ";
        }
        // line 14
        echo "                
            </div>
            <div class=\"col-md-4\">
                
                <h1> ";
        // line 18
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["appartement"]) || array_key_exists("appartement", $context) ? $context["appartement"] : (function () { throw new RuntimeError('Variable "appartement" does not exist.', 18, $this->source); })()), "quartier", [], "any", false, false, false, 18), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["appartement"]) || array_key_exists("appartement", $context) ? $context["appartement"] : (function () { throw new RuntimeError('Variable "appartement" does not exist.', 18, $this->source); })()), "ville", [], "any", false, false, false, 18), "html", null, true);
        echo ")</h1>
                <h2><div class=\"text-primary\">";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["appartement"]) || array_key_exists("appartement", $context) ? $context["appartement"] : (function () { throw new RuntimeError('Variable "appartement" does not exist.', 19, $this->source); })()), "prix", [], "any", false, false, false, 19), "html", null, true);
        echo " FCFA </div></h2>
            </div>
        </div>
    </div>
</div>
</div>
<div class=\"container\">
        <p>";
        // line 26
        echo nl2br(twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["appartement"]) || array_key_exists("appartement", $context) ? $context["appartement"] : (function () { throw new RuntimeError('Variable "appartement" does not exist.', 26, $this->source); })()), "descrition", [], "any", false, false, false, 26), "html", null, true));
        echo "</p>

        <div class= \"row \">
             <div class=\"col-md-12\">
                <h2>Carateristiques</h2>
                <table class= \"table table-striped \">
                    
                    <tr>   
                        <td>Nombre de pièces </td>
                        <td>";
        // line 35
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["appartement"]) || array_key_exists("appartement", $context) ? $context["appartement"] : (function () { throw new RuntimeError('Variable "appartement" does not exist.', 35, $this->source); })()), "nbPieces", [], "any", false, false, false, 35), "html", null, true);
        echo "</td>
                    </tr>
                    <tr>   
                        <td>Chambres</td>
                        <td>";
        // line 39
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["appartement"]) || array_key_exists("appartement", $context) ? $context["appartement"] : (function () { throw new RuntimeError('Variable "appartement" does not exist.', 39, $this->source); })()), "nbreChambres", [], "any", false, false, false, 39), "html", null, true);
        echo "</td>
                    </tr>
                    
                     <tr>   
                        <td>type</td>
                        <td>";
        // line 44
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["appartement"]) || array_key_exists("appartement", $context) ? $context["appartement"] : (function () { throw new RuntimeError('Variable "appartement" does not exist.', 44, $this->source); })()), "types", [], "any", false, false, false, 44), "html", null, true);
        echo "</td>
                    </tr>
                </table> 
            </div>
</div>
    </div>
    <div class=\"row\">
        <div class=\"col-sm-12 text-right\">
                <button class=\" btn btn-warning btn-sm ml-2\">
                    Modifier
                    <span><i class=\"fa fa-pencil\" aria-hidden=\"true\"> </i> </span>
                </button>
                <a href=\"";
        // line 56
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("biens_index");
        echo "\" class=\"btn btn-info btn-sm mr-4\">
                    <span><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i> </span>
                    Retour à la liste
                </a>
        </div>
    </div>
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "appartement/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 56,  135 => 44,  127 => 39,  120 => 35,  108 => 26,  98 => 19,  92 => 18,  86 => 14,  81 => 12,  78 => 11,  76 => 10,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin.html.twig' %}

{% block body %}

<div class=\"jumbotron text-center \">
   <div class=\"container\">
        <div class =\"row\">
            <div class=\"col-md-8\">
                
                {% if appartement.filename %}
                   
                    <img src=\"{{ asset('uploads/appartement/' ~ appartement.filename) }}\" alt=\"img-card-top\"  style=\"width: 100%;height: 100%;\">
                {% endif %}
                
            </div>
            <div class=\"col-md-4\">
                
                <h1> {{appartement.quartier}} ({{appartement.ville}})</h1>
                <h2><div class=\"text-primary\">{{appartement.prix}} FCFA </div></h2>
            </div>
        </div>
    </div>
</div>
</div>
<div class=\"container\">
        <p>{{appartement.descrition|nl2br}}</p>

        <div class= \"row \">
             <div class=\"col-md-12\">
                <h2>Carateristiques</h2>
                <table class= \"table table-striped \">
                    
                    <tr>   
                        <td>Nombre de pièces </td>
                        <td>{{appartement.nbPieces}}</td>
                    </tr>
                    <tr>   
                        <td>Chambres</td>
                        <td>{{appartement.nbreChambres}}</td>
                    </tr>
                    
                     <tr>   
                        <td>type</td>
                        <td>{{appartement.types}}</td>
                    </tr>
                </table> 
            </div>
</div>
    </div>
    <div class=\"row\">
        <div class=\"col-sm-12 text-right\">
                <button class=\" btn btn-warning btn-sm ml-2\">
                    Modifier
                    <span><i class=\"fa fa-pencil\" aria-hidden=\"true\"> </i> </span>
                </button>
                <a href=\"{{ path('biens_index') }}\" class=\"btn btn-info btn-sm mr-4\">
                    <span><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i> </span>
                    Retour à la liste
                </a>
        </div>
    </div>
</div>

{% endblock %} 
", "appartement/show.html.twig", "E:\\Work\\siems\\Siem immo\\templates\\appartement\\show.html.twig");
    }
}
