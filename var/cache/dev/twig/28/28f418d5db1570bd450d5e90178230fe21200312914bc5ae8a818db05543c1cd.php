<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* biens/_form.html.twig */
class __TwigTemplate_c6c998c1321125881f84be00419ff6979eb13155f93f5a3d7bdd86884949fbe0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "biens/_form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "biens/_form.html.twig"));

        // line 1
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 1, $this->source); })()), 'form_start');
        echo "
<div class =\"mt-4 offset-1\">
    <div class=\"row\">
            <div class=\"col-md-4\">";
        // line 4
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 4, $this->source); })()), "proprietaires", [], "any", false, false, false, 4), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
            <div class=\"col-md-4\">";
        // line 5
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 5, $this->source); })()), "types", [], "any", false, false, false, 5), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
            
    </div>
    <div class=\"row\">
        <div class=\"col-md-4\">";
        // line 9
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 9, $this->source); })()), "typeGestions", [], "any", false, false, false, 9), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
           
            <div class=\"col-md-4\">";
        // line 11
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 11, $this->source); })()), "prix", [], "any", false, false, false, 11), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
    </div>
    <div class=\"row\">
            <div class=\"col-md-4\">";
        // line 14
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 14, $this->source); })()), "ville", [], "any", false, false, false, 14), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
            <div class=\"col-md-4\">";
        // line 15
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 15, $this->source); })()), "quartier", [], "any", false, false, false, 15), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
    </div>
    <div class=\"row\">
            <div class=\"col-md-4\">";
        // line 18
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 18, $this->source); })()), "nbrePieces", [], "any", false, false, false, 18), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
            <div class=\"col-md-4\">";
        // line 19
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 19, $this->source); })()), "nbrechambre", [], "any", false, false, false, 19), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
    </div>   
    <div class=\"col-lg-8\">";
        // line 21
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 21, $this->source); })()), "description", [], "any", false, false, false, 21), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div >  
    
     
    <button class=\" btn ";
        // line 24
        echo twig_escape_filter($this->env, (((isset($context["button_color"]) || array_key_exists("button_color", $context))) ? (_twig_default_filter((isset($context["button_color"]) || array_key_exists("button_color", $context) ? $context["button_color"] : (function () { throw new RuntimeError('Variable "button_color" does not exist.', 24, $this->source); })()), "btn-success")) : ("btn-success")), "html", null, true);
        echo " offset-2 btn-sm\" ><span><i class=\"fa fa-floppy-o\" aria-hidden=\"true\"> </i> ";
        echo twig_escape_filter($this->env, (((isset($context["button_label"]) || array_key_exists("button_label", $context))) ? (_twig_default_filter((isset($context["button_label"]) || array_key_exists("button_label", $context) ? $context["button_label"] : (function () { throw new RuntimeError('Variable "button_label" does not exist.', 24, $this->source); })()), "Enregistrer")) : ("Enregistrer")), "html", null, true);
        echo "</span></button>
    <a href=\"";
        // line 25
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("proprietaire_index");
        echo "\" class=\"btn btn-info btn-sm ml-2\"><span><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i> </span>Retour à la liste</a>
</div>     
";
        // line 27
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 27, $this->source); })()), 'form_end');
        echo "




";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "biens/_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 27,  102 => 25,  96 => 24,  90 => 21,  85 => 19,  81 => 18,  75 => 15,  71 => 14,  65 => 11,  60 => 9,  53 => 5,  49 => 4,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ form_start(form) }}
<div class =\"mt-4 offset-1\">
    <div class=\"row\">
            <div class=\"col-md-4\">{{form_row(form.proprietaires, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
            <div class=\"col-md-4\">{{form_row(form.types, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
            
    </div>
    <div class=\"row\">
        <div class=\"col-md-4\">{{form_row(form.typeGestions, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
           
            <div class=\"col-md-4\">{{form_row(form.prix, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
    </div>
    <div class=\"row\">
            <div class=\"col-md-4\">{{form_row(form.ville, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
            <div class=\"col-md-4\">{{form_row(form.quartier, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
    </div>
    <div class=\"row\">
            <div class=\"col-md-4\">{{form_row(form.nbrePieces, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
            <div class=\"col-md-4\">{{form_row(form.nbrechambre, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
    </div>   
    <div class=\"col-lg-8\">{{form_row(form.description, {'attr': {'class': \"form-control form-control-sm\"}})}}</div >  
    
     
    <button class=\" btn {{ button_color|default('btn-success') }} offset-2 btn-sm\" ><span><i class=\"fa fa-floppy-o\" aria-hidden=\"true\"> </i> {{ button_label|default('Enregistrer') }}</span></button>
    <a href=\"{{ path('proprietaire_index') }}\" class=\"btn btn-info btn-sm ml-2\"><span><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i> </span>Retour à la liste</a>
</div>     
{{ form_end(form) }}




", "biens/_form.html.twig", "E:\\Work\\siems\\Siem immo\\templates\\biens\\_form.html.twig");
    }
}
