<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home.html.twig */
class __TwigTemplate_bfde9b4c81f3acb9b1946148c590515ddb42973339a51a076af84070c2857855 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "home.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<style>
  .centered {
      text-align: center;

    }
    .bloc {
      min-height: 300px;
      margin-bottom: 15px;
    }
    .zp_img{
      width: 100%;
      height: 200px;
      overflow: hidden;
    }
    .zp_zdesc{
        border: 1px solid #ccc;
        border-radius: 2px;
        padding-bottom: 10px;
    }
    .item_zp{
        margin-bottom: 20px;
    }
    
  .prix3{
      display: block;
      cursor: pointer;
      text-align: center;
      width: 80%;
      margin: auto auto;
      padding: 10px;
      border: 1px solid #81BC24;
      border-radius: 5px;
      background-color: #81BC24;
      color: white;
      font-weight: 700;
      margin-bottom: 10px;
  }
  .details3{
      display: block;
      cursor: pointer;
      text-align: center;
      width: 80px;
      margin: auto auto;
      padding: 10px;
      border: 1px solid #F91425;
      border-radius: 5px;
      background-color: #F91425;
      color: white;
      font-weight: 700;
      margin-bottom: 10px;
      margin-top: 10px;
      text-transform: uppercase;
  }
  

</style>

<div class=\"z_back\">
    <img src=\"home_background.jpg\" alt=\"photo\" style=\"width: 100%;height:500px;\" class=\"mb-2\"/>
</div>
<div>
    <form class=\"row  offset-1\">
      <input type=\"text\" class=\" ml-4 col-md-2 form-control\" placeholder=\"ville\"/>
      <input type=\"text\" class=\"col-md-2 ml-4 form-control\" placeholder=\"quartier\"/>
      <input type=\"text\" class=\"col-md-2 ml-4 form-control\" placeholder=\"prix maximun\"/>
      <input type=\"text\" class=\" col-md-2 ml-4 form-control\" placeholder=\"nombre de maison\"/>
      <input type=\"submit\" class=\" ml-4 btn btn-info\">
    </form>
</div>
  
<div class = \"container\">
  
  <h1 class=\"mt-4\" style=\"text-align: center;\">Nos  Nouveautés</h1>
  <div class = \"row flex mt-4\">
      ";
        // line 78
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["appartements"]) || array_key_exists("appartements", $context) ? $context["appartements"] : (function () { throw new RuntimeError('Variable "appartements" does not exist.', 78, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["appartement"]) {
            // line 79
            echo "        ";
            // line 91
            echo "        <div class=\"col-md-6 col-sm-4 centered bloc\">
            <div class=\"item_zp\">
                <div class=\"zp_img\">
                    ";
            // line 95
            echo "                </div>
                <div class=\"zp_zdesc\">
                    <p class=\"card-text mb-1\" style = \"font-size: 1rem; font-weight:bold;\">";
            // line 97
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["appartement"], "quartier", [], "any", false, false, false, 97), "html", null, true);
            echo " (";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["appartement"], "ville", [], "any", false, false, false, 97), "html", null, true);
            echo ")</p>
                    <p class=\"card-text mb-1 \" style = \"font-size: 1rem; font-weight:bold;\">";
            // line 98
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["appartement"], "prix", [], "any", false, false, false, 98), "html", null, true);
            echo " FCFA</p>
                    
                </div>
            </div>
        </div>

      ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['appartement'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 104
        echo "   
  </div>
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 104,  165 => 98,  159 => 97,  155 => 95,  150 => 91,  148 => 79,  144 => 78,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
<style>
  .centered {
      text-align: center;

    }
    .bloc {
      min-height: 300px;
      margin-bottom: 15px;
    }
    .zp_img{
      width: 100%;
      height: 200px;
      overflow: hidden;
    }
    .zp_zdesc{
        border: 1px solid #ccc;
        border-radius: 2px;
        padding-bottom: 10px;
    }
    .item_zp{
        margin-bottom: 20px;
    }
    
  .prix3{
      display: block;
      cursor: pointer;
      text-align: center;
      width: 80%;
      margin: auto auto;
      padding: 10px;
      border: 1px solid #81BC24;
      border-radius: 5px;
      background-color: #81BC24;
      color: white;
      font-weight: 700;
      margin-bottom: 10px;
  }
  .details3{
      display: block;
      cursor: pointer;
      text-align: center;
      width: 80px;
      margin: auto auto;
      padding: 10px;
      border: 1px solid #F91425;
      border-radius: 5px;
      background-color: #F91425;
      color: white;
      font-weight: 700;
      margin-bottom: 10px;
      margin-top: 10px;
      text-transform: uppercase;
  }
  

</style>

<div class=\"z_back\">
    <img src=\"home_background.jpg\" alt=\"photo\" style=\"width: 100%;height:500px;\" class=\"mb-2\"/>
</div>
<div>
    <form class=\"row  offset-1\">
      <input type=\"text\" class=\" ml-4 col-md-2 form-control\" placeholder=\"ville\"/>
      <input type=\"text\" class=\"col-md-2 ml-4 form-control\" placeholder=\"quartier\"/>
      <input type=\"text\" class=\"col-md-2 ml-4 form-control\" placeholder=\"prix maximun\"/>
      <input type=\"text\" class=\" col-md-2 ml-4 form-control\" placeholder=\"nombre de maison\"/>
      <input type=\"submit\" class=\" ml-4 btn btn-info\">
    </form>
</div>
  
<div class = \"container\">
  
  <h1 class=\"mt-4\" style=\"text-align: center;\">Nos  Nouveautés</h1>
  <div class = \"row flex mt-4\">
      {% for appartement in appartements %}
        {#<div class = \"card ml-4 col-md-4\">
          {% if appartement.filename %}
              <img src=\"{{ asset('uploads/appartement/' ~ appartement.filename) }}\" alt=\"img-card-top\" style=\"width:100%; height:50%; \">
          {% endif %}
          <div class=\"card-body\">
              
              <p class=\"card-text\">{{appartement.quartier}} ({{appartement.ville}})</p>
              <p class=\"card-text text-primary\" style = \"font-size: 2rem; font-weight:bold;\">{{appartement.prix}} FCFA</p>
              {#<a href=\"{{path('home_show',{ id : appartement.id})}}\" class=\"btn  btn-sm btn-info\" >Détail</a>
          </div>
        </div>
        #}
        <div class=\"col-md-6 col-sm-4 centered bloc\">
            <div class=\"item_zp\">
                <div class=\"zp_img\">
                    {#<a href=\"{{path('detail',{ id : appartement.id})}}\" > <img src=\"{{ asset('uploads/appartement/' ~ appartement.filename) }}\"  style=\"width: 100%;height: 200px;\"/></a>#}
                </div>
                <div class=\"zp_zdesc\">
                    <p class=\"card-text mb-1\" style = \"font-size: 1rem; font-weight:bold;\">{{appartement.quartier}} ({{appartement.ville}})</p>
                    <p class=\"card-text mb-1 \" style = \"font-size: 1rem; font-weight:bold;\">{{appartement.prix}} FCFA</p>
                    
                </div>
            </div>
        </div>

      {%  endfor %}   
  </div>
</div>

{% endblock %} 
", "home.html.twig", "E:\\Work\\siems\\Siem immo\\templates\\home.html.twig");
    }
}
