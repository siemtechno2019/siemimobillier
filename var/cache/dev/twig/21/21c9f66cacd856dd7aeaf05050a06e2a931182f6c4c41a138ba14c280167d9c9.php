<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* owner/parcelles.html.twig */
class __TwigTemplate_a38108e0ee5a138eb44879a04d021f39298fa374d2b22e629473033b9977e470 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "owner.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "owner/parcelles.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "owner/parcelles.html.twig"));

        $this->parent = $this->loadTemplate("owner.html.twig", "owner/parcelles.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<div class = \"container\">
    <h1>Liste des parcelles</h1> 
    
    <table class=\"table table-stripped\">
        <thead>
            <tr>
                <th>identifiant </th>
                <th>description </th>
                <th>prix </th>
                <th>vendu </th>
                <th>Actions </th>
            </tr>
        </thead>
        <tbody>
            ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["parcelles"]) || array_key_exists("parcelles", $context) ? $context["parcelles"] : (function () { throw new RuntimeError('Variable "parcelles" does not exist.', 19, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["parcelle"]) {
            // line 20
            echo "            <tr>
              <td>";
            // line 21
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["parcelle"], "identifiant", [], "any", false, false, false, 21), "html", null, true);
            echo "</td>
              <td>";
            // line 22
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["parcelle"], "ville", [], "any", false, false, false, 22), "html", null, true);
            echo "</td>
              <td>";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["parcelle"], "quartier", [], "any", false, false, false, 23), "html", null, true);
            echo "</td>
              <td>";
            // line 24
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["parcelle"], "prix", [], "any", false, false, false, 24), "html", null, true);
            echo "</td>
              <td>";
            // line 25
            if (twig_get_attribute($this->env, $this->source, $context["parcelle"], "vendu", [], "any", false, false, false, 25)) {
                echo "OUI";
            } else {
                echo "NON";
            }
            echo "</td>
              <td><a href=\"";
            // line 26
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("owner_show", ["id" => twig_get_attribute($this->env, $this->source, $context["parcelle"], "id", [], "any", false, false, false, 26)]), "html", null, true);
            echo "\" class = \"btn btn-info btn-sm\"><i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\"></i>détail</a>
            </tr>
           
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['parcelle'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "        </tbody>
    </table>
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "owner/parcelles.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 30,  116 => 26,  108 => 25,  104 => 24,  100 => 23,  96 => 22,  92 => 21,  89 => 20,  85 => 19,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'owner.html.twig' %}

{% block body %}

<div class = \"container\">
    <h1>Liste des parcelles</h1> 
    
    <table class=\"table table-stripped\">
        <thead>
            <tr>
                <th>identifiant </th>
                <th>description </th>
                <th>prix </th>
                <th>vendu </th>
                <th>Actions </th>
            </tr>
        </thead>
        <tbody>
            {% for parcelle in parcelles %}
            <tr>
              <td>{{parcelle.identifiant}}</td>
              <td>{{parcelle.ville}}</td>
              <td>{{parcelle.quartier}}</td>
              <td>{{parcelle.prix}}</td>
              <td>{% if parcelle.vendu %}OUI{% else %}NON{% endif%}</td>
              <td><a href=\"{{ path('owner_show', {'id': parcelle.id}) }}\" class = \"btn btn-info btn-sm\"><i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\"></i>détail</a>
            </tr>
           
            {% endfor %}
        </tbody>
    </table>
</div>

{% endblock %} 
", "owner/parcelles.html.twig", "E:\\Work\\siems\\Siem immo\\templates\\owner\\parcelles.html.twig");
    }
}
