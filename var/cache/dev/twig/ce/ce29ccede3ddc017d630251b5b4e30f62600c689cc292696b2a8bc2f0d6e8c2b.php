<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* parcelle/_form.html.twig */
class __TwigTemplate_95fddf3d1aaa45755370c435d52052e9534099d6906e9e65d8ce3998047b053c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "parcelle/_form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "parcelle/_form.html.twig"));

        // line 1
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 1, $this->source); })()), 'form_start');
        echo "
    <div class =\"\"> 
        <div class=\"row\">
            <div class=\"col-md-6\">";
        // line 4
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 4, $this->source); })()), "identifiant", [], "any", false, false, false, 4), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
            <div class=\"col-md-6\">";
        // line 5
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 5, $this->source); })()), "description", [], "any", false, false, false, 5), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
        </div>  
        <div class=\"row\">
            <div class=\"col-md-6\">";
        // line 8
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 8, $this->source); })()), "ville", [], "any", false, false, false, 8), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
            <div class=\"col-md-6\">";
        // line 9
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 9, $this->source); })()), "quartier", [], "any", false, false, false, 9), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
        </div>  
        <div class=\"row\">
            <div class=\"col-md-6\">";
        // line 12
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 12, $this->source); })()), "prix", [], "any", false, false, false, 12), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
            <div class=\"col-md-6\">";
        // line 13
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 13, $this->source); })()), "proprietaires", [], "any", false, false, false, 13), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
        </div>  
       <div class=\"col-md-1 mt-4\">";
        // line 15
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 15, $this->source); })()), "vendu", [], "any", false, false, false, 15), 'row');
        echo "</div>
        <div class=\"ml-4\">
            <button class = \"btn btn-success mb-4\">";
        // line 17
        echo twig_escape_filter($this->env, (((isset($context["button"]) || array_key_exists("button", $context))) ? (_twig_default_filter((isset($context["button"]) || array_key_exists("button", $context) ? $context["button"] : (function () { throw new RuntimeError('Variable "button" does not exist.', 17, $this->source); })()), "Enregistrer")) : ("Enregistrer")), "html", null, true);
        echo "</button>
            <a href=\"";
        // line 18
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("parcelle_index");
        echo "\" class=\"btn btn-primary mb-4 \" style =\" display :inline-block\">Retour à la liste</a>
        </div>
        
    </div> 
    
    ";
        // line 23
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 23, $this->source); })()), 'rest');
        echo "
";
        // line 24
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 24, $this->source); })()), 'form_end');
        echo "

    
   ";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "parcelle/_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 24,  95 => 23,  87 => 18,  83 => 17,  78 => 15,  73 => 13,  69 => 12,  63 => 9,  59 => 8,  53 => 5,  49 => 4,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ form_start(form) }}
    <div class =\"\"> 
        <div class=\"row\">
            <div class=\"col-md-6\">{{form_row(form.identifiant, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
            <div class=\"col-md-6\">{{form_row(form.description, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
        </div>  
        <div class=\"row\">
            <div class=\"col-md-6\">{{form_row(form.ville, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
            <div class=\"col-md-6\">{{form_row(form.quartier, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
        </div>  
        <div class=\"row\">
            <div class=\"col-md-6\">{{form_row(form.prix, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
            <div class=\"col-md-6\">{{form_row(form.proprietaires, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
        </div>  
       <div class=\"col-md-1 mt-4\">{{form_row(form.vendu)}}</div>
        <div class=\"ml-4\">
            <button class = \"btn btn-success mb-4\">{{button|default('Enregistrer')}}</button>
            <a href=\"{{ path('parcelle_index') }}\" class=\"btn btn-primary mb-4 \" style =\" display :inline-block\">Retour à la liste</a>
        </div>
        
    </div> 
    
    {{  form_rest(form)}}
{{ form_end(form) }}

    
   ", "parcelle/_form.html.twig", "E:\\Work\\siems\\Siem immo\\templates\\parcelle\\_form.html.twig");
    }
}
