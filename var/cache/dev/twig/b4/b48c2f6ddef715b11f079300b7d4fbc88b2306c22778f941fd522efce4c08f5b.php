<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* appartement/_form.html.twig */
class __TwigTemplate_9eb6c55f5690d5a354b640d32db2b0aa7dd868ea49a8dde976f3e5c0809034c0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "appartement/_form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "appartement/_form.html.twig"));

        // line 1
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 1, $this->source); })()), 'form_start');
        echo "
    
    <div class = \"row\">
        <div class=\"col-md-5\">";
        // line 4
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 4, $this->source); })()), "proprietaire", [], "any", false, false, false, 4), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
        <div class=\"col-md-5\">";
        // line 5
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 5, $this->source); })()), "identifiant", [], "any", false, false, false, 5), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
        
    </div>
    <div class =\"row\"> 
            <div class=\"col-md-5\">";
        // line 9
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 9, $this->source); })()), "ville", [], "any", false, false, false, 9), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>   
            
            <div class=\"col-md-5\">";
        // line 11
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 11, $this->source); })()), "quartier", [], "any", false, false, false, 11), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
        </div> 
    <div class =\"row\">    
        <div class=\"col-md-5\">";
        // line 14
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 14, $this->source); })()), "nbPieces", [], "any", false, false, false, 14), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
        <div class=\"col-md-5\">";
        // line 15
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 15, $this->source); })()), "nbreChambres", [], "any", false, false, false, 15), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
    </div> 
    
    <div class =\"row\">    
        <div class=\"col-md-5\">";
        // line 19
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 19, $this->source); })()), "types", [], "any", false, false, false, 19), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
        <div class=\"col-md-5\">";
        // line 20
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 20, $this->source); })()), "locataires", [], "any", false, false, false, 20), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
    </div> 
    <div class =\"row\">    
        <div class=\"col-md-5\">";
        // line 23
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 23, $this->source); })()), "conditionLocation", [], "any", false, false, false, 23), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
        <div class=\"col-md-5\">";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 24, $this->source); })()), "descrition", [], "any", false, false, false, 24), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
        
        ";
        // line 27
        echo "    </div> 
    <div class =\"row\">
        <div class=\"col-md-5\">";
        // line 29
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 29, $this->source); })()), "prix", [], "any", false, false, false, 29), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
        <div class=\"col-md-1\">";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 30, $this->source); })()), "louer", [], "any", false, false, false, 30), 'row');
        echo "</div>
    </div>
    
    ";
        // line 33
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 33, $this->source); })()), 'rest');
        echo "

    <button class = \"btn btn-success mb-4\">";
        // line 35
        echo twig_escape_filter($this->env, (((isset($context["button"]) || array_key_exists("button", $context))) ? (_twig_default_filter((isset($context["button"]) || array_key_exists("button", $context) ? $context["button"] : (function () { throw new RuntimeError('Variable "button" does not exist.', 35, $this->source); })()), "Enregistrer")) : ("Enregistrer")), "html", null, true);
        echo "</button>
    <a href=\"";
        // line 36
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("appartement_index");
        echo "\" class=\"btn btn-primary mb-4\" style =\" display :inline-block\">Retour à la liste</a>

";
        // line 38
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 38, $this->source); })()), 'form_end');
        echo "
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "appartement/_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  129 => 38,  124 => 36,  120 => 35,  115 => 33,  109 => 30,  105 => 29,  101 => 27,  96 => 24,  92 => 23,  86 => 20,  82 => 19,  75 => 15,  71 => 14,  65 => 11,  60 => 9,  53 => 5,  49 => 4,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ form_start(form) }}
    
    <div class = \"row\">
        <div class=\"col-md-5\">{{form_row(form.proprietaire, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
        <div class=\"col-md-5\">{{form_row(form.identifiant, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
        
    </div>
    <div class =\"row\"> 
            <div class=\"col-md-5\">{{form_row(form.ville, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>   
            
            <div class=\"col-md-5\">{{form_row(form.quartier, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
        </div> 
    <div class =\"row\">    
        <div class=\"col-md-5\">{{form_row(form.nbPieces, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
        <div class=\"col-md-5\">{{form_row(form.nbreChambres, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
    </div> 
    
    <div class =\"row\">    
        <div class=\"col-md-5\">{{form_row(form.types, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
        <div class=\"col-md-5\">{{form_row(form.locataires, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
    </div> 
    <div class =\"row\">    
        <div class=\"col-md-5\">{{form_row(form.conditionLocation, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
        <div class=\"col-md-5\">{{form_row(form.descrition, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
        
        {#<div class=\"col-md-5\">{{form_row(form.imageFile, {'attr': {'class': \"custom-file-input\",'lang':\"fr\"}})}}</div> #}
    </div> 
    <div class =\"row\">
        <div class=\"col-md-5\">{{form_row(form.prix, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
        <div class=\"col-md-1\">{{form_row(form.louer)}}</div>
    </div>
    
    {{  form_rest(form)}}

    <button class = \"btn btn-success mb-4\">{{button|default('Enregistrer')}}</button>
    <a href=\"{{ path('appartement_index') }}\" class=\"btn btn-primary mb-4\" style =\" display :inline-block\">Retour à la liste</a>

{{ form_end(form) }}
", "appartement/_form.html.twig", "E:\\Work\\siems\\Siem immo\\templates\\appartement\\_form.html.twig");
    }
}
