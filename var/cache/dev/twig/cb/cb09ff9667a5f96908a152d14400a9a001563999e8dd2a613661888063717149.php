<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* owner/index.html.twig */
class __TwigTemplate_4d8ea9e47d1da80a7ae71b608ed61b8c60efcaa90e1fe5e41872bd29ca8078c9 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "owner.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "owner/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "owner/index.html.twig"));

        $this->parent = $this->loadTemplate("owner.html.twig", "owner/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<div class = \"container\">
    <h1>Gestion des biens</h1> 
    
    <table class=\"table table-stripped\">
        <thead>
            <tr>
                <th>Biens </th>
                <th>Ville </th>
                <th>Quartier </th>
                <th>Prix </th>
                <th>Louer </th>
                <th>Actions </th>
                
            </tr>
        </thead>
        <tbody>
            ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["appartements"]) || array_key_exists("appartements", $context) ? $context["appartements"] : (function () { throw new RuntimeError('Variable "appartements" does not exist.', 21, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["appartement"]) {
            // line 22
            echo "            <tr>
              <td>";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["appartement"], "identifiant", [], "any", false, false, false, 23), "html", null, true);
            echo "</td>
              <td>";
            // line 24
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["appartement"], "ville", [], "any", false, false, false, 24), "html", null, true);
            echo "</td>
              <td>";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["appartement"], "quartier", [], "any", false, false, false, 25), "html", null, true);
            echo "</td>
              <td>";
            // line 26
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["appartement"], "prix", [], "any", false, false, false, 26), "html", null, true);
            echo "</td>
              <td>";
            // line 27
            if (twig_get_attribute($this->env, $this->source, $context["appartement"], "louer", [], "any", false, false, false, 27)) {
                echo "OUI";
            } else {
                echo "NON";
            }
            echo "</td>
              <td><a href=\"";
            // line 28
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("owner_show", ["id" => twig_get_attribute($this->env, $this->source, $context["appartement"], "id", [], "any", false, false, false, 28)]), "html", null, true);
            echo "\" class = \"btn btn-info btn-sm\"><i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\"></i>détail</a>
            </tr>
           
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['appartement'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "        </tbody>
    </table>
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "owner/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 32,  118 => 28,  110 => 27,  106 => 26,  102 => 25,  98 => 24,  94 => 23,  91 => 22,  87 => 21,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'owner.html.twig' %}

{% block body %}

<div class = \"container\">
    <h1>Gestion des biens</h1> 
    
    <table class=\"table table-stripped\">
        <thead>
            <tr>
                <th>Biens </th>
                <th>Ville </th>
                <th>Quartier </th>
                <th>Prix </th>
                <th>Louer </th>
                <th>Actions </th>
                
            </tr>
        </thead>
        <tbody>
            {% for appartement in appartements %}
            <tr>
              <td>{{appartement.identifiant}}</td>
              <td>{{appartement.ville}}</td>
              <td>{{appartement.quartier}}</td>
              <td>{{appartement.prix}}</td>
              <td>{% if appartement.louer %}OUI{% else %}NON{% endif%}</td>
              <td><a href=\"{{ path('owner_show', {'id': appartement.id}) }}\" class = \"btn btn-info btn-sm\"><i class=\"fa fa-pencil-square-o\" aria-hidden=\"true\"></i>détail</a>
            </tr>
           
            {% endfor %}
        </tbody>
    </table>
</div>

{% endblock %} 
", "owner/index.html.twig", "E:\\Work\\siems\\Siem immo\\templates\\owner\\index.html.twig");
    }
}
