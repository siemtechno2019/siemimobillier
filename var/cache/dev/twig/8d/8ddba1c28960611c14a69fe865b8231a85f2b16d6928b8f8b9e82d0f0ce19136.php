<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* biens/show.html.twig */
class __TwigTemplate_72037c0fb91c657f7133bc8b81a66c401292ea57f070bd47adc4feeef421ae6a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "biens/show.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "biens/show.html.twig"));

        $this->parent = $this->loadTemplate("admin.html.twig", "biens/show.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<div class=\"jumbotron text-center \">
   <div class=\"container\">
        <div class =\"row\">
            <div class=\"col-md-8\">
                ";
        // line 14
        echo "            </div>
            <div class=\"col-md-4\">
                
                <h1> ";
        // line 17
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["bien"]) || array_key_exists("bien", $context) ? $context["bien"] : (function () { throw new RuntimeError('Variable "bien" does not exist.', 17, $this->source); })()), "quartier", [], "any", false, false, false, 17), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["bien"]) || array_key_exists("bien", $context) ? $context["bien"] : (function () { throw new RuntimeError('Variable "bien" does not exist.', 17, $this->source); })()), "ville", [], "any", false, false, false, 17), "html", null, true);
        echo ")</h1>
                <h2><div class=\"text-primary\">";
        // line 18
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["bien"]) || array_key_exists("bien", $context) ? $context["bien"] : (function () { throw new RuntimeError('Variable "bien" does not exist.', 18, $this->source); })()), "prix", [], "any", false, false, false, 18), "html", null, true);
        echo " FCFA </div></h2>
            </div>
        </div>
    </div>
</div>
</div>
<div class=\"container\">
        <p>";
        // line 25
        echo nl2br(twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["bien"]) || array_key_exists("bien", $context) ? $context["bien"] : (function () { throw new RuntimeError('Variable "bien" does not exist.', 25, $this->source); })()), "description", [], "any", false, false, false, 25), "html", null, true));
        echo "</p>

        <div class= \"row \">
             <div class=\"col-md-12\">
                <h2>Carateristiques</h2>
                <table class= \"table table-striped \">
                    
                    <tr>   
                        <td>Nombre de pièces </td>
                        <td>";
        // line 34
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["bien"]) || array_key_exists("bien", $context) ? $context["bien"] : (function () { throw new RuntimeError('Variable "bien" does not exist.', 34, $this->source); })()), "nbrePieces", [], "any", false, false, false, 34), "html", null, true);
        echo "</td>
                    </tr>
                    <tr>   
                        <td>Chambres</td>
                        <td>";
        // line 38
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["bien"]) || array_key_exists("bien", $context) ? $context["bien"] : (function () { throw new RuntimeError('Variable "bien" does not exist.', 38, $this->source); })()), "nbrechambre", [], "any", false, false, false, 38), "html", null, true);
        echo "</td>
                    </tr>
                    <tr>   
                        <td>Type de gestion</td>
                        <td>";
        // line 42
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["bien"]) || array_key_exists("bien", $context) ? $context["bien"] : (function () { throw new RuntimeError('Variable "bien" does not exist.', 42, $this->source); })()), "typeGestions", [], "any", false, false, false, 42), "html", null, true);
        echo "</td>
                    </tr>
                     <tr>   
                        <td>type</td>
                        <td>";
        // line 46
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["bien"]) || array_key_exists("bien", $context) ? $context["bien"] : (function () { throw new RuntimeError('Variable "bien" does not exist.', 46, $this->source); })()), "types", [], "any", false, false, false, 46), "html", null, true);
        echo "</td>
                    </tr>
                </table> 
            </div>
</div>
    </div>
    <div class=\"row\">
        <div class=\"col-sm-12 text-right\">
                <button class=\" btn btn-warning btn-sm ml-2\">
                    Modifier
                    <span><i class=\"fa fa-pencil\" aria-hidden=\"true\"> </i> </span>
                </button>
                <a href=\"";
        // line 58
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("biens_index");
        echo "\" class=\"btn btn-info btn-sm mr-4\">
                    <span><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i> </span>
                    Retour à la liste
                </a>
        </div>
    </div>
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "biens/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  144 => 58,  129 => 46,  122 => 42,  115 => 38,  108 => 34,  96 => 25,  86 => 18,  80 => 17,  75 => 14,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin.html.twig' %}

{% block body %}

<div class=\"jumbotron text-center \">
   <div class=\"container\">
        <div class =\"row\">
            <div class=\"col-md-8\">
                {#
                {% if property.filename %}
                    <img src=\"{{vich_uploader_asset(property,'imageFile') | imagine_filter('meduim')}}\" alt=\"img-card-top\" >
                {% endif %}
                #}
            </div>
            <div class=\"col-md-4\">
                
                <h1> {{bien.quartier}} ({{bien.ville}})</h1>
                <h2><div class=\"text-primary\">{{bien.prix}} FCFA </div></h2>
            </div>
        </div>
    </div>
</div>
</div>
<div class=\"container\">
        <p>{{bien.description|nl2br}}</p>

        <div class= \"row \">
             <div class=\"col-md-12\">
                <h2>Carateristiques</h2>
                <table class= \"table table-striped \">
                    
                    <tr>   
                        <td>Nombre de pièces </td>
                        <td>{{bien.nbrePieces}}</td>
                    </tr>
                    <tr>   
                        <td>Chambres</td>
                        <td>{{bien.nbrechambre}}</td>
                    </tr>
                    <tr>   
                        <td>Type de gestion</td>
                        <td>{{bien.typeGestions}}</td>
                    </tr>
                     <tr>   
                        <td>type</td>
                        <td>{{bien.types}}</td>
                    </tr>
                </table> 
            </div>
</div>
    </div>
    <div class=\"row\">
        <div class=\"col-sm-12 text-right\">
                <button class=\" btn btn-warning btn-sm ml-2\">
                    Modifier
                    <span><i class=\"fa fa-pencil\" aria-hidden=\"true\"> </i> </span>
                </button>
                <a href=\"{{ path('biens_index') }}\" class=\"btn btn-info btn-sm mr-4\">
                    <span><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i> </span>
                    Retour à la liste
                </a>
        </div>
    </div>
</div>

{% endblock %} 
", "biens/show.html.twig", "E:\\Work\\siems\\Siem immo\\templates\\biens\\show.html.twig");
    }
}
