<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* security/login.html.twig */
class __TwigTemplate_fdb5f21db1f06da556d36e0277f0da43592806ed1d486d9a40135cce8239f7c8 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    
<head>
        <title>Matrix Admin</title><meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
\t\t<link rel=\"stylesheet\" href=\"css/bootstrap.min.css\" />
\t\t<link rel=\"stylesheet\" href=\"css/bootstrap-responsive.min.css\" />
        <link rel=\"stylesheet\" href=\"css/matrix-login.css\" />
        <link href=\"font-awesome/css/font-awesome.css\" rel=\"stylesheet\" />
\t\t<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

    </head>
    <body>
        <div id=\"loginbox\">            
            <form id=\"loginform\" class=\"form-vertical\" method=\"post\">
                ";
        // line 17
        if ((isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new RuntimeError('Variable "error" does not exist.', 17, $this->source); })())) {
            // line 18
            echo "                    <div class=\"alert alert-danger\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new RuntimeError('Variable "error" does not exist.', 18, $this->source); })()), "messageKey", [], "any", false, false, false, 18), twig_get_attribute($this->env, $this->source, (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new RuntimeError('Variable "error" does not exist.', 18, $this->source); })()), "messageData", [], "any", false, false, false, 18), "security"), "html", null, true);
            echo "</div>
                ";
        }
        // line 20
        echo "\t\t\t\t <div class=\"control-group normal_text\"> <h3>SIEM IMMOBILIER</div>
                <div class=\"control-group\">
                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lg\"><i class=\"icon-user\"> </i></span><input type=\"email\" value=\"";
        // line 24
        echo twig_escape_filter($this->env, (isset($context["last_username"]) || array_key_exists("last_username", $context) ? $context["last_username"] : (function () { throw new RuntimeError('Variable "last_username" does not exist.', 24, $this->source); })()), "html", null, true);
        echo "\" name=\"email\" id=\"inputEmail\" 
                            class=\"form-control class=\"col-md-4\"\" placeholder=\"Email\" required autofocus>
                        </div>
                    </div>
                </div>
                <div class=\"control-group\">
                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_ly\"><i class=\"icon-lock\"></i></span><input type=\"password\" name=\"password\" id=\"inputPassword\"
                             class=\"form-control class=\"col-md-4\"\" placeholder=\"Mot de Passe\" required>
                        </div>
                    </div>
                </div>
                <input type=\"hidden\" name=\"_csrf_token\"
                value=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken("authenticate"), "html", null, true);
        echo "\"
                >  
                <div class=\"form-actions\">
                    
                    <span style=\"margin-left: 150px;\"><button type=\"submit\" class=\"btn btn-success btn-sm mt-3\">connexion</button></span>
                </div>
            </form>
            <form id=\"recoverform\" action=\"#\" class=\"form-vertical\">
\t\t\t\t<p class=\"normal_text\">Enter your e-mail address below and we will send you instructions how to recover a password.</p>
\t\t\t\t
                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lo\"><i class=\"icon-envelope\"></i></span><input type=\"text\" placeholder=\"E-mail address\" />
                        </div>
                    </div>
               
                <div class=\"form-actions\">
                    <span class=\"pull-left\"><a href=\"#\" class=\"flip-link btn btn-success\" id=\"to-login\">&laquo; Back to login</a></span>
                    <span class=\"pull-right\"><a class=\"btn btn-info\"/>Reecover</a></span>
                </div>
            </form>
        </div>
        
        <script src=\"js/jquery.min.js\"></script>  
        <script src=\"js/matrix.login.js\"></script> 
    </body>

</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 38,  75 => 24,  69 => 20,  63 => 18,  61 => 17,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html lang=\"en\">
    
<head>
        <title>Matrix Admin</title><meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />
\t\t<link rel=\"stylesheet\" href=\"css/bootstrap.min.css\" />
\t\t<link rel=\"stylesheet\" href=\"css/bootstrap-responsive.min.css\" />
        <link rel=\"stylesheet\" href=\"css/matrix-login.css\" />
        <link href=\"font-awesome/css/font-awesome.css\" rel=\"stylesheet\" />
\t\t<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

    </head>
    <body>
        <div id=\"loginbox\">            
            <form id=\"loginform\" class=\"form-vertical\" method=\"post\">
                {% if error %}
                    <div class=\"alert alert-danger\">{{ error.messageKey|trans(error.messageData, 'security') }}</div>
                {% endif %}
\t\t\t\t <div class=\"control-group normal_text\"> <h3>SIEM IMMOBILIER</div>
                <div class=\"control-group\">
                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lg\"><i class=\"icon-user\"> </i></span><input type=\"email\" value=\"{{ last_username }}\" name=\"email\" id=\"inputEmail\" 
                            class=\"form-control class=\"col-md-4\"\" placeholder=\"Email\" required autofocus>
                        </div>
                    </div>
                </div>
                <div class=\"control-group\">
                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_ly\"><i class=\"icon-lock\"></i></span><input type=\"password\" name=\"password\" id=\"inputPassword\"
                             class=\"form-control class=\"col-md-4\"\" placeholder=\"Mot de Passe\" required>
                        </div>
                    </div>
                </div>
                <input type=\"hidden\" name=\"_csrf_token\"
                value=\"{{ csrf_token('authenticate') }}\"
                >  
                <div class=\"form-actions\">
                    
                    <span style=\"margin-left: 150px;\"><button type=\"submit\" class=\"btn btn-success btn-sm mt-3\">connexion</button></span>
                </div>
            </form>
            <form id=\"recoverform\" action=\"#\" class=\"form-vertical\">
\t\t\t\t<p class=\"normal_text\">Enter your e-mail address below and we will send you instructions how to recover a password.</p>
\t\t\t\t
                    <div class=\"controls\">
                        <div class=\"main_input_box\">
                            <span class=\"add-on bg_lo\"><i class=\"icon-envelope\"></i></span><input type=\"text\" placeholder=\"E-mail address\" />
                        </div>
                    </div>
               
                <div class=\"form-actions\">
                    <span class=\"pull-left\"><a href=\"#\" class=\"flip-link btn btn-success\" id=\"to-login\">&laquo; Back to login</a></span>
                    <span class=\"pull-right\"><a class=\"btn btn-info\"/>Reecover</a></span>
                </div>
            </form>
        </div>
        
        <script src=\"js/jquery.min.js\"></script>  
        <script src=\"js/matrix.login.js\"></script> 
    </body>

</html>
", "security/login.html.twig", "E:\\Work\\siems\\Siem immo\\templates\\security\\login.html.twig");
    }
}
