<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* proprietaire/_form.html.twig */
class __TwigTemplate_57153415d2aa2c8d56b5d9115ea9f548341f6fd8ff4e57671e19ee454f2871de extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "proprietaire/_form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "proprietaire/_form.html.twig"));

        // line 1
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 1, $this->source); })()), 'form_start');
        echo "
<div class =\"mt-4 offset-2\">
        <div class=\"col-md-8\">";
        // line 3
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 3, $this->source); })()), "nom", [], "any", false, false, false, 3), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
        <div class=\"col-md-8\">";
        // line 4
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 4, $this->source); })()), "adresse", [], "any", false, false, false, 4), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
        <div class=\"col-md-8\">";
        // line 5
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 5, $this->source); })()), "contact", [], "any", false, false, false, 5), 'row', ["attr" => ["class" => "form-control form-control-sm"]]);
        echo "</div>
     
    <button class=\" btn ";
        // line 7
        echo twig_escape_filter($this->env, (((isset($context["button_color"]) || array_key_exists("button_color", $context))) ? (_twig_default_filter((isset($context["button_color"]) || array_key_exists("button_color", $context) ? $context["button_color"] : (function () { throw new RuntimeError('Variable "button_color" does not exist.', 7, $this->source); })()), "btn-success")) : ("btn-success")), "html", null, true);
        echo " offset-2 btn-sm\" ><span><i class=\"fa fa-floppy-o\" aria-hidden=\"true\"> </i> ";
        echo twig_escape_filter($this->env, (((isset($context["button_label"]) || array_key_exists("button_label", $context))) ? (_twig_default_filter((isset($context["button_label"]) || array_key_exists("button_label", $context) ? $context["button_label"] : (function () { throw new RuntimeError('Variable "button_label" does not exist.', 7, $this->source); })()), "Enregistrer")) : ("Enregistrer")), "html", null, true);
        echo "</span></button>
    <a href=\"";
        // line 8
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("proprietaire_index");
        echo "\" class=\"btn btn-info btn-sm ml-2 mb-5\"><span><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i> </span>Retour à la liste</a>
</div>     
";
        // line 10
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 10, $this->source); })()), 'form_end');
        echo "
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "proprietaire/_form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 10,  67 => 8,  61 => 7,  56 => 5,  52 => 4,  48 => 3,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{{ form_start(form) }}
<div class =\"mt-4 offset-2\">
        <div class=\"col-md-8\">{{form_row(form.nom, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
        <div class=\"col-md-8\">{{form_row(form.adresse, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
        <div class=\"col-md-8\">{{form_row(form.contact, {'attr': {'class': \"form-control form-control-sm\"}})}}</div>
     
    <button class=\" btn {{ button_color|default('btn-success') }} offset-2 btn-sm\" ><span><i class=\"fa fa-floppy-o\" aria-hidden=\"true\"> </i> {{ button_label|default('Enregistrer') }}</span></button>
    <a href=\"{{ path('proprietaire_index') }}\" class=\"btn btn-info btn-sm ml-2 mb-5\"><span><i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i> </span>Retour à la liste</a>
</div>     
{{ form_end(form) }}
", "proprietaire/_form.html.twig", "E:\\Work\\siems\\Siem immo\\templates\\proprietaire\\_form.html.twig");
    }
}
