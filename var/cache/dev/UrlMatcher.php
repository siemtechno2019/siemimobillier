<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/admin/appartement' => [[['_route' => 'appartement_index', '_controller' => 'App\\Controller\\AppartementController::index'], null, ['GET' => 0], null, true, false, null]],
        '/admin/appartement/new' => [[['_route' => 'appartement_new', '_controller' => 'App\\Controller\\AppartementController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/admin/biens' => [[['_route' => 'biens_index', '_controller' => 'App\\Controller\\BiensController::index'], null, ['GET' => 0], null, true, false, null]],
        '/admin/biens/new' => [[['_route' => 'biens_new', '_controller' => 'App\\Controller\\BiensController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/' => [[['_route' => 'home', '_controller' => 'App\\Controller\\HomeController::index'], null, null, null, false, false, null]],
        '/parcelles' => [[['_route' => 'home_parcelles', '_controller' => 'App\\Controller\\HomeController::homeParcelles'], null, null, null, false, false, null]],
        '/vente' => [[['_route' => 'home_vente', '_controller' => 'App\\Controller\\HomeController::homeVente'], null, null, null, false, false, null]],
        '/allLocation' => [[['_route' => 'home_location', '_controller' => 'App\\Controller\\HomeLocationController::index'], null, null, null, false, false, null]],
        '/admin/locataire' => [[['_route' => 'locataire_index', '_controller' => 'App\\Controller\\LocataireController::index'], null, ['GET' => 0], null, true, false, null]],
        '/admin/locataire/new' => [[['_route' => 'locataire_new', '_controller' => 'App\\Controller\\LocataireController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/owner' => [[['_route' => 'owner', '_controller' => 'App\\Controller\\OwnerController::index'], null, null, null, false, false, null]],
        '/owner/parcelles' => [[['_route' => 'owner_parcelle', '_controller' => 'App\\Controller\\OwnerController::parcelleIndex'], null, null, null, false, false, null]],
        '/admin/paiement' => [[['_route' => 'paiement_index', '_controller' => 'App\\Controller\\PaiementController::index'], null, ['GET' => 0], null, true, false, null]],
        '/admin/paiement/new' => [[['_route' => 'paiement_new', '_controller' => 'App\\Controller\\PaiementController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/admin/parcelle' => [[['_route' => 'parcelle_index', '_controller' => 'App\\Controller\\ParcelleController::index'], null, ['GET' => 0], null, true, false, null]],
        '/admin/parcelle/new' => [[['_route' => 'parcelle_new', '_controller' => 'App\\Controller\\ParcelleController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/admin/proprietaire' => [[['_route' => 'proprietaire_index', '_controller' => 'App\\Controller\\ProprietaireController::index'], null, ['GET' => 0], null, true, false, null]],
        '/admin/proprietaire/new' => [[['_route' => 'proprietaire_new', '_controller' => 'App\\Controller\\ProprietaireController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/inscription' => [[['_route' => 'app_register', '_controller' => 'App\\Controller\\RegistrationController::register'], null, null, null, false, false, null]],
        '/admin/reglement' => [[['_route' => 'reglement_index', '_controller' => 'App\\Controller\\ReglementController::index'], null, ['GET' => 0], null, true, false, null]],
        '/admin/reglement/new' => [[['_route' => 'reglement_new', '_controller' => 'App\\Controller\\ReglementController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/login' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, ['GET' => 0], null, false, false, null]],
        '/admin/type' => [[['_route' => 'type_index', '_controller' => 'App\\Controller\\TypeController::index'], null, ['GET' => 0], null, true, false, null]],
        '/admin/type/new' => [[['_route' => 'type_new', '_controller' => 'App\\Controller\\TypeController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/admin/type_gestion' => [[['_route' => 'type_gestion_index', '_controller' => 'App\\Controller\\TypeGestionController::index'], null, ['GET' => 0], null, true, false, null]],
        '/admin/type_gestion/new' => [[['_route' => 'type_gestion_new', '_controller' => 'App\\Controller\\TypeGestionController::new'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
                .'|/admin/(?'
                    .'|appartement/([^/]++)(?'
                        .'|(*:202)'
                        .'|/edit(*:215)'
                        .'|(*:223)'
                    .')'
                    .'|biens/([^/]++)(?'
                        .'|(*:249)'
                        .'|/edit(*:262)'
                        .'|(*:270)'
                    .')'
                    .'|locataire/([^/]++)(?'
                        .'|(*:300)'
                        .'|/edit(*:313)'
                        .'|(*:321)'
                    .')'
                    .'|p(?'
                        .'|a(?'
                            .'|iement/([^/]++)(?'
                                .'|(*:356)'
                                .'|/edit(*:369)'
                                .'|(*:377)'
                            .')'
                            .'|rcelle/([^/]++)(?'
                                .'|(*:404)'
                                .'|/edit(*:417)'
                                .'|(*:425)'
                            .')'
                        .')'
                        .'|roprietaire/([^/]++)(?'
                            .'|(*:458)'
                            .'|/edit(*:471)'
                            .'|(*:479)'
                        .')'
                    .')'
                    .'|reglement/([^/]++)(?'
                        .'|(*:510)'
                        .'|/edit(*:523)'
                        .'|(*:531)'
                    .')'
                    .'|type(?'
                        .'|/([^/]++)(?'
                            .'|(*:559)'
                            .'|/edit(*:572)'
                            .'|(*:580)'
                        .')'
                        .'|_gestion/([^/]++)(?'
                            .'|(*:609)'
                            .'|/edit(*:622)'
                            .'|(*:630)'
                        .')'
                    .')'
                .')'
                .'|/parcelles/detail/([^/]++)(*:667)'
                .'|/vente/detail/([^/]++)(*:697)'
                .'|/detail/([^/]++)(*:721)'
                .'|/show/owner/([^/]++)(*:749)'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_twig_error_test', '_controller' => 'twig.controller.preview_error::previewErrorPageAction', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception::showAction'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception::cssAction'], ['token'], null, null, false, false, null]],
        159 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        202 => [[['_route' => 'appartement_show', '_controller' => 'App\\Controller\\AppartementController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        215 => [[['_route' => 'appartement_edit', '_controller' => 'App\\Controller\\AppartementController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        223 => [[['_route' => 'appartement_delete', '_controller' => 'App\\Controller\\AppartementController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        249 => [[['_route' => 'biens_show', '_controller' => 'App\\Controller\\BiensController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        262 => [[['_route' => 'biens_edit', '_controller' => 'App\\Controller\\BiensController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        270 => [[['_route' => 'biens_delete', '_controller' => 'App\\Controller\\BiensController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        300 => [[['_route' => 'locataire_show', '_controller' => 'App\\Controller\\LocataireController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        313 => [[['_route' => 'locataire_edit', '_controller' => 'App\\Controller\\LocataireController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        321 => [[['_route' => 'locataire_delete', '_controller' => 'App\\Controller\\LocataireController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        356 => [[['_route' => 'paiement_show', '_controller' => 'App\\Controller\\PaiementController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        369 => [[['_route' => 'paiement_edit', '_controller' => 'App\\Controller\\PaiementController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        377 => [[['_route' => 'paiement_delete', '_controller' => 'App\\Controller\\PaiementController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        404 => [[['_route' => 'parcelle_show', '_controller' => 'App\\Controller\\ParcelleController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        417 => [[['_route' => 'parcelle_edit', '_controller' => 'App\\Controller\\ParcelleController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        425 => [[['_route' => 'parcelle_delete', '_controller' => 'App\\Controller\\ParcelleController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        458 => [[['_route' => 'proprietaire_show', '_controller' => 'App\\Controller\\ProprietaireController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        471 => [[['_route' => 'proprietaire_edit', '_controller' => 'App\\Controller\\ProprietaireController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        479 => [[['_route' => 'proprietaire_delete', '_controller' => 'App\\Controller\\ProprietaireController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        510 => [[['_route' => 'reglement_show', '_controller' => 'App\\Controller\\ReglementController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        523 => [[['_route' => 'reglement_edit', '_controller' => 'App\\Controller\\ReglementController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        531 => [[['_route' => 'reglement_delete', '_controller' => 'App\\Controller\\ReglementController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        559 => [[['_route' => 'type_show', '_controller' => 'App\\Controller\\TypeController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        572 => [[['_route' => 'type_edit', '_controller' => 'App\\Controller\\TypeController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        580 => [[['_route' => 'type_delete', '_controller' => 'App\\Controller\\TypeController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        609 => [[['_route' => 'type_gestion_show', '_controller' => 'App\\Controller\\TypeGestionController::show'], ['id'], ['GET' => 0], null, false, true, null]],
        622 => [[['_route' => 'type_gestion_edit', '_controller' => 'App\\Controller\\TypeGestionController::edit'], ['id'], ['GET' => 0, 'POST' => 1], null, false, false, null]],
        630 => [[['_route' => 'type_gestion_delete', '_controller' => 'App\\Controller\\TypeGestionController::delete'], ['id'], ['DELETE' => 0], null, false, true, null]],
        667 => [[['_route' => 'detail_parcelles', '_controller' => 'App\\Controller\\HomeController::detailParcelle'], ['id'], ['GET' => 0], null, false, true, null]],
        697 => [[['_route' => 'detail', '_controller' => 'App\\Controller\\HomeController::detailBien'], ['id'], ['GET' => 0], null, false, true, null]],
        721 => [[['_route' => 'location_detail', '_controller' => 'App\\Controller\\HomeLocationController::detail'], ['id'], ['GET' => 0], null, false, true, null]],
        749 => [
            [['_route' => 'owner_show', '_controller' => 'App\\Controller\\OwnerController::show'], ['id'], ['GET' => 0], null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
