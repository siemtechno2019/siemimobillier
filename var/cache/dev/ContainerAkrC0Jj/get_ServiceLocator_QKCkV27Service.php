<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private '.service_locator.QKCkV27' shared service.

return $this->privates['.service_locator.QKCkV27'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($this->getService, [
    'biens' => ['privates', '.errored..service_locator.QKCkV27.App\\Entity\\Biens', NULL, 'Cannot autowire service ".service_locator.QKCkV27": it references class "App\\Entity\\Biens" but no such service exists.'],
], [
    'biens' => 'App\\Entity\\Biens',
]);
