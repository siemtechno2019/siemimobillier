<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private '.service_locator.UFjgI9c' shared service.

return $this->privates['.service_locator.UFjgI9c'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($this->getService, [
    'paiementRepository' => ['privates', 'App\\Repository\\PaiementRepository', 'getPaiementRepositoryService.php', true],
], [
    'paiementRepository' => 'App\\Repository\\PaiementRepository',
]);
