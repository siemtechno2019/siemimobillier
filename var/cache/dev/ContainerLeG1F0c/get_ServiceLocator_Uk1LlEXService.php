<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private '.service_locator.Uk1LlEX' shared service.

return $this->privates['.service_locator.Uk1LlEX'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($this->getService, [
    'appartementRepository' => ['privates', 'App\\Repository\\AppartementRepository', 'getAppartementRepositoryService.php', true],
], [
    'appartementRepository' => 'App\\Repository\\AppartementRepository',
]);
