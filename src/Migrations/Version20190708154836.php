<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190708154836 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE biens (id INT AUTO_INCREMENT NOT NULL, types_id INT DEFAULT NULL, type_gestions_id INT DEFAULT NULL, proprietaires_id INT DEFAULT NULL, description LONGTEXT NOT NULL, nbre_pieces INT NOT NULL, nbrechambre INT NOT NULL, prix INT NOT NULL, ville VARCHAR(255) NOT NULL, quartier VARCHAR(255) NOT NULL, superficie VARCHAR(255) NOT NULL, INDEX IDX_1F9004DD8EB23357 (types_id), INDEX IDX_1F9004DDAE8F244D (type_gestions_id), INDEX IDX_1F9004DD710ED0A5 (proprietaires_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE biens ADD CONSTRAINT FK_1F9004DD8EB23357 FOREIGN KEY (types_id) REFERENCES type (id)');
        $this->addSql('ALTER TABLE biens ADD CONSTRAINT FK_1F9004DDAE8F244D FOREIGN KEY (type_gestions_id) REFERENCES type_gestion (id)');
        $this->addSql('ALTER TABLE biens ADD CONSTRAINT FK_1F9004DD710ED0A5 FOREIGN KEY (proprietaires_id) REFERENCES proprietaire (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE biens');
    }
}
