<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190717085317 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE reglement (id INT AUTO_INCREMENT NOT NULL, mode_de_reglement VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE appartement CHANGE types_id types_id INT DEFAULT NULL, CHANGE locataires_id locataires_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE biens CHANGE types_id types_id INT DEFAULT NULL, CHANGE type_gestions_id type_gestions_id INT DEFAULT NULL, CHANGE proprietaires_id proprietaires_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE paiement ADD reglements_id INT DEFAULT NULL, CHANGE locataires_id locataires_id INT DEFAULT NULL, CHANGE appartements_id appartements_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE paiement ADD CONSTRAINT FK_B1DC7A1E8DABDAA6 FOREIGN KEY (reglements_id) REFERENCES reglement (id)');
        $this->addSql('CREATE INDEX IDX_B1DC7A1E8DABDAA6 ON paiement (reglements_id)');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE paiement DROP FOREIGN KEY FK_B1DC7A1E8DABDAA6');
        $this->addSql('DROP TABLE reglement');
        $this->addSql('ALTER TABLE appartement CHANGE types_id types_id INT DEFAULT NULL, CHANGE locataires_id locataires_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE biens CHANGE types_id types_id INT DEFAULT NULL, CHANGE type_gestions_id type_gestions_id INT DEFAULT NULL, CHANGE proprietaires_id proprietaires_id INT DEFAULT NULL');
        $this->addSql('DROP INDEX IDX_B1DC7A1E8DABDAA6 ON paiement');
        $this->addSql('ALTER TABLE paiement DROP reglements_id, CHANGE locataires_id locataires_id INT DEFAULT NULL, CHANGE appartements_id appartements_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT NOT NULL COLLATE utf8mb4_bin');
    }
}
