<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190802110524 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE biens ADD vendu TINYINT(1) NOT NULL, CHANGE types_id types_id INT DEFAULT NULL, CHANGE type_gestions_id type_gestions_id INT DEFAULT NULL, CHANGE proprietaires_id proprietaires_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE appartement CHANGE types_id types_id INT DEFAULT NULL, CHANGE locataires_id locataires_id INT DEFAULT NULL, CHANGE proprietaire_id proprietaire_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE parcelle CHANGE proprietaires_id proprietaires_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE paiement CHANGE locataires_id locataires_id INT DEFAULT NULL, CHANGE appartements_id appartements_id INT DEFAULT NULL, CHANGE reglements_id reglements_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE image_file CHANGE appartement_id appartement_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE appartement CHANGE types_id types_id INT DEFAULT NULL, CHANGE locataires_id locataires_id INT DEFAULT NULL, CHANGE proprietaire_id proprietaire_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE biens DROP vendu, CHANGE types_id types_id INT DEFAULT NULL, CHANGE type_gestions_id type_gestions_id INT DEFAULT NULL, CHANGE proprietaires_id proprietaires_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE image_file CHANGE appartement_id appartement_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE paiement CHANGE locataires_id locataires_id INT DEFAULT NULL, CHANGE appartements_id appartements_id INT DEFAULT NULL, CHANGE reglements_id reglements_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE parcelle CHANGE proprietaires_id proprietaires_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT NOT NULL COLLATE utf8mb4_bin');
    }
}
