<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190724084530 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE parcelle (id INT AUTO_INCREMENT NOT NULL, proprietaires_id INT DEFAULT NULL, identifiant VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, prix INT NOT NULL, INDEX IDX_C56E2CF6710ED0A5 (proprietaires_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE parcelle ADD CONSTRAINT FK_C56E2CF6710ED0A5 FOREIGN KEY (proprietaires_id) REFERENCES proprietaire (id)');
        $this->addSql('ALTER TABLE appartement CHANGE types_id types_id INT DEFAULT NULL, CHANGE locataires_id locataires_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE biens CHANGE types_id types_id INT DEFAULT NULL, CHANGE type_gestions_id type_gestions_id INT DEFAULT NULL, CHANGE proprietaires_id proprietaires_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE paiement CHANGE locataires_id locataires_id INT DEFAULT NULL, CHANGE appartements_id appartements_id INT DEFAULT NULL, CHANGE reglements_id reglements_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE parcelle');
        $this->addSql('ALTER TABLE appartement CHANGE types_id types_id INT DEFAULT NULL, CHANGE locataires_id locataires_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE biens CHANGE types_id types_id INT DEFAULT NULL, CHANGE type_gestions_id type_gestions_id INT DEFAULT NULL, CHANGE proprietaires_id proprietaires_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE paiement CHANGE locataires_id locataires_id INT DEFAULT NULL, CHANGE appartements_id appartements_id INT DEFAULT NULL, CHANGE reglements_id reglements_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT NOT NULL COLLATE utf8mb4_bin');
    }
}
