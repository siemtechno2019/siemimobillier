<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190711105411 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE biens CHANGE types_id types_id INT DEFAULT NULL, CHANGE type_gestions_id type_gestions_id INT DEFAULT NULL, CHANGE proprietaires_id proprietaires_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD nom VARCHAR(255) NOT NULL, ADD date_naissance VARCHAR(255) NOT NULL, ADD adresse VARCHAR(255) NOT NULL, ADD telephone INT NOT NULL, CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE biens CHANGE types_id types_id INT DEFAULT NULL, CHANGE type_gestions_id type_gestions_id INT DEFAULT NULL, CHANGE proprietaires_id proprietaires_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user DROP nom, DROP date_naissance, DROP adresse, DROP telephone, CHANGE roles roles LONGTEXT NOT NULL COLLATE utf8mb4_bin');
    }
}
