<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BiensRepository")
 */
class Biens
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbrePieces;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbrechambre;

    /**
     * @ORM\Column(type="integer")
     */
    private $prix;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $quartier;

    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Type", inversedBy="biens")
     */
    private $types;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeGestion", inversedBy="biens")
     */
    private $typeGestions;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Proprietaire", inversedBy="biens")
     */
    private $proprietaires;

    /**
     * @ORM\Column(type="boolean")
     */
    private $vendu;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNbrePieces(): ?int
    {
        return $this->nbrePieces;
    }

    public function setNbrePieces(int $nbrePieces): self
    {
        $this->nbrePieces = $nbrePieces;

        return $this;
    }

    public function getNbrechambre(): ?int
    {
        return $this->nbrechambre;
    }

    public function setNbrechambre(int $nbrechambre): self
    {
        $this->nbrechambre = $nbrechambre;

        return $this;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getQuartier(): ?string
    {
        return $this->quartier;
    }

    public function setQuartier(string $quartier): self
    {
        $this->quartier = $quartier;

        return $this;
    }

   
    public function getTypes(): ?Type
    {
        return $this->types;
    }

    public function setTypes(?Type $types): self
    {
        $this->types = $types;

        return $this;
    }

    public function getTypeGestions(): ?TypeGestion
    {
        return $this->typeGestions;
    }

    public function setTypeGestions(?TypeGestion $typeGestions): self
    {
        $this->typeGestions = $typeGestions;

        return $this;
    }

    public function getProprietaires(): ?Proprietaire
    {
        return $this->proprietaires;
    }

    public function setProprietaires(?Proprietaire $proprietaires): self
    {
        $this->proprietaires = $proprietaires;

        return $this;
    }

    public function getVendu(): ?bool
    {
        return $this->vendu;
    }

    public function setVendu(bool $vendu): self
    {
        $this->vendu = $vendu;

        return $this;
    }
}
