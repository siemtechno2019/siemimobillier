<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PaiementRepository")
 */
class Paiement
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Locataire", inversedBy="paiements")
     */
    private $locataires;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Appartement", inversedBy="paiements")
     */
    private $appartements;

    /**
     * @ORM\Column(type="date")
     */
    private $datePaiement;

    /**
     * @ORM\Column(type="integer")
     */
    private $Montant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Reglement", inversedBy="paiements")
     */
    private $reglements;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLocataires(): ?Locataire
    {
        return $this->locataires;
    }

    public function setLocataires(?Locataire $locataires): self
    {
        $this->locataires = $locataires;

        return $this;
    }

    public function getAppartements(): ?Appartement
    {
        return $this->appartements;
    }

    public function setAppartements(?Appartement $appartements): self
    {
        $this->appartements = $appartements;

        return $this;
    }

    public function getDatePaiement(): ?\DateTimeInterface
    {
        return $this->datePaiement;
    }

    public function setDatePaiement(\DateTimeInterface $datePaiement): self
    {
        $this->datePaiement = $datePaiement;

        return $this;
    }

    public function getMontant(): ?int
    {
        return $this->Montant;
    }

    public function setMontant(int $Montant): self
    {
        $this->Montant = $Montant;

        return $this;
    }

    public function getReglements(): ?Reglement
    {
        return $this->reglements;
    }

    public function setReglements(?Reglement $reglements): self
    {
        $this->reglements = $reglements;

        return $this;
    }
}
