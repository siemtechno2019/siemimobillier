<?php

namespace App\Repository;

use App\Entity\TypeGestion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TypeGestion|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeGestion|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeGestion[]    findAll()
 * @method TypeGestion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeGestionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TypeGestion::class);
    }

    // /**
    //  * @return TypeGestion[] Returns an array of TypeGestion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeGestion
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
