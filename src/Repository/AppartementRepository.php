<?php

namespace App\Repository;

use App\Entity\Appartement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Appartement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Appartement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Appartement[]    findAll()
 * @method Appartement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AppartementRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Appartement::class);
    }

    public function findLatest()
    {
        return $this->createQueryBuilder('a')
            ->Where('a.louer = false')
            ->setMaxResults(6)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findOwner($user)
    {
        return $this->createQueryBuilder('a')
        ->join('a.proprietaire','p')
        ->andWhere('p.nom = :val')
        ->setParameter('val', $user->getNom())
        ->getQuery()
        ->getResult()
        ;
    }
    
    public function findVisible()
    {
        return $this->createQueryBuilder('a')
            ->Where('a.louer = false')
            ->getQuery()
            ->getResult()
        ;
    }
    
    

    // /**
    //  * @return Appartement[] Returns an array of Appartement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Appartement
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
