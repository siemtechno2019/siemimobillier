<?php

namespace App\Form;

use App\Entity\Appartement;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;


class AppartementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('proprietaire')
            ->add('identifiant')
            ->add('descrition')
            ->add('nbPieces')
            ->add('nbreChambres')
            ->add('prix')
            ->add('quartier')
            ->add('ville')
            ->add('conditionLocation')
            ->add('types')
            ->add('locataires')
            ->add('louer')
            //->add('imageFile',FileType::class,['required'=>false])
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Appartement::class,
        ]);
    }
}
