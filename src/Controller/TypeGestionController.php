<?php

namespace App\Controller;

use App\Entity\TypeGestion;
use App\Form\TypeGestionType;
use App\Repository\TypeGestionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/type_gestion")
 */
class TypeGestionController extends AbstractController
{
    /**
     * @Route("/", name="type_gestion_index", methods={"GET"})
     */
    public function index(TypeGestionRepository $typeGestionRepository): Response
    {
        return $this->render('type_gestion/index.html.twig', [
            'type_gestions' => $typeGestionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="type_gestion_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $typeGestion = new TypeGestion();
        $form = $this->createForm(TypeGestionType::class, $typeGestion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($typeGestion);
            $entityManager->flush();

            return $this->redirectToRoute('type_gestion_index');
        }

        return $this->render('type_gestion/new.html.twig', [
            'type_gestion' => $typeGestion,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_gestion_show", methods={"GET"})
     */
    public function show(TypeGestion $typeGestion): Response
    {
        return $this->render('type_gestion/show.html.twig', [
            'type_gestion' => $typeGestion,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="type_gestion_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TypeGestion $typeGestion): Response
    {
        $form = $this->createForm(TypeGestionType::class, $typeGestion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('type_gestion_index', [
                'id' => $typeGestion->getId(),
            ]);
        }

        return $this->render('type_gestion/edit.html.twig', [
            'type_gestion' => $typeGestion,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="type_gestion_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TypeGestion $typeGestion): Response
    {
        if ($this->isCsrfTokenValid('delete'.$typeGestion->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($typeGestion);
            $entityManager->flush();
        }

        return $this->redirectToRoute('type_gestion_index');
    }
}
