<?php

namespace App\Controller;

use App\Entity\Appartement;
use App\Repository\AppartementRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeLocationController extends AbstractController
{
    /**
     * @Route("/allLocation", name="home_location")
     */
    public function index(AppartementRepository $repository)
    {
        $appartements = $repository->findVisible();
        return $this->render('home_location/index.html.twig',[
        
        'appartements' => $appartements
       ]);
    }

    /**
     * @Route("detail/{id}", name="location_detail", methods={"GET"})
     */
    public function detail(Appartement $appartement): Response
    {
        //dd($appartement);
        return $this->render('home/detail.html.twig', [
            'appartement' => $appartement,
        ]);
    }


}

    


