<?php

namespace App\Controller;

use App\Entity\Biens;
use App\Entity\Parcelle;
use App\Entity\Appartement;
use App\Repository\BiensRepository;
use App\Repository\ParcelleRepository;
use App\Repository\AppartementRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(AppartementRepository $repository)
    {
        $appartements = $repository->findLatest();
        return $this->render('home.html.twig',[
        
        'appartements' => $appartements
       ]);
    }

    /**
     * @Route("/parcelles", name="home_parcelles")
     */
    public function homeParcelles(ParcelleRepository $repository)
    {
        $parcelles = $repository->findLatest();
        return $this->render('home/parcelles.html.twig',[
        
        'parcelles' => $parcelles
       ]);
    }

    /**
     * @Route("detail/{id}", name="detail", methods={"GET"})
     */
    public function detail(Appartement $appartement): Response
    {
        //dd($appartement);
        return $this->render('home/detail.html.twig', [
            'appartement' => $appartement,
        ]);
    }

    /**
     * @Route("/parcelles/detail/{id}", name="detail_parcelles", methods={"GET"})
     */
    public function detailParcelle(Parcelle $parcelle): Response
    {
        //dd($appartement);
        return $this->render('home/detail.html.twig', [
            'parcelle' => $parcelle,
        ]);
    }

    /**
     * @Route("/vente", name="home_vente")
     */
    public function homeVente(BiensRepository $repository)
    {
        $biens = $repository->findLatest();
        return $this->render('home/vente.html.twig',[
        
            'biens' => $biens
       ]);
    }


    /**
     * @Route("/vente/detail/{id}", name="detail", methods={"GET"})
     */
    public function detailBien(Biens $biens): Response
    {
        
        return $this->render('home/detail.html.twig', [
            'bien' => $bien,
        ]);
    }

}

    


