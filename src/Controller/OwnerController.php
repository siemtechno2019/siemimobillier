<?php

namespace App\Controller;

use App\Entity\Appartement;
use App\Repository\ParcelleRepository;
use App\Repository\AppartementRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class OwnerController extends AbstractController
{
    /**
     * @Route("/owner", name="owner")
     */
    public function index(AppartementRepository $repository)
    {
        $user = $this->getUser();
        //dd($user);  
        $appartements = $repository->findOwner($user);
        //dd($appartements);
        return $this->render('owner/index.html.twig',[
            'appartements' => $appartements
       ]);
        
    }

    /**
     * @Route("/owner/parcelles", name="owner_parcelle")
     */
    public function parcelleIndex(ParcelleRepository $repository)
    {
        $user = $this->getUser();
        //dd($user);  
        $parcelles = $repository->findOwner($user);
        //dd($appartements);
        return $this->render('owner/parcelles.html.twig',[
            'parcelles' => $parcelles
        ]);
        
    }

    /**
     * @Route("show/owner/{id}", name="owner_show", methods={"GET"})
     */
    public function show(Appartement $appartement): Response
    {
        //dd($appartement);
        return $this->render('owner/show.html.twig', [
            'appartement' => $appartement,
        ]);
    }




}
